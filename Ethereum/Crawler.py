import json
import requests
import time

class EthereumCrawler(object):
    def __init__(self,
                 rest_endpoint = "https://mainnet.infura.io/v3/ab537b8c21814d24824678afe85b4f0c",
                 ws_endpoint = "wss://mainnet.infura.io/ws",
                 delay = 0.1
                 ):
        self.headers = {"content-type": "application/json"}
        self.__rest_endpoint = rest_endpoint
        self.__ws_endpoint = ws_endpoint
        self.delay = delay

    def _rpcRequest(self, method, params, key):
        time.sleep(self.delay)
        payload = {
            "method": method,
            "params": params,
            "jsonrpc": "2.0",
            "id": 0
        }
        res = requests.post(
              self.__rest_endpoint,
              data=json.dumps(payload),
              headers=self.headers).json()
        return res[key]

    def getBlockByNumber(self, n):
        return self._rpcRequest("eth_getBlockByNumber", [hex(n), True], "result")

    # def getTransactionsByBlockNumber(self, n):
    #     block = self.getBlockByNumber(n)
    #     return block['transactions']

    def getBlockNumber(self):
        return int(self._rpcRequest("eth_blockNumber", [], "result"), 16)