from collections import namedtuple

Pattern = namedtuple('Pattern', ['key', 'value'])

def matchPattern(element:dict, patterns):

    def found(element, key, value):
        if isinstance(value, set):
            if element.get(key) in value:
                return True
        else:
            if element.get(key) == value:
                return True
        return False

    for key, value in patterns:
        if isinstance(key, list):
            res = False
            for k in key:
                if found(element, k, value):
                    res = True
                    break
            if not res:
                return False
        else:
            if not found(element, key, value):
                return False
    return True