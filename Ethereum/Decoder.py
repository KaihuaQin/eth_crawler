import json
from ethereum.abi import (
    decode_abi,
    normalize_name as normalize_abi_method_name,
    method_id as get_abi_method_id)
from ethereum.utils import encode_int, zpad, decode_hex

class InputDecoder(object):
    def __init__(self, abi):
        with open(abi, 'r') as f:
            self._abi = json.load(f)
        self.func_map = {}
        self.func_name = {}
        for description in self._abi:
            if description.get('type') != 'function':
                continue
            method_name = normalize_abi_method_name(description['name'])
            arg_types = [item['type'] for item in description['inputs']]
            method_id = get_abi_method_id(method_name, arg_types)
            method_signature = zpad(encode_int(method_id), 4)
            self.func_name[method_signature] = method_name
            self.func_map[method_signature] = arg_types

    def get_func_name(self, call_data):
        return self.func_name.get(decode_hex(call_data)[:4])

    def decode_call(self, call_data):
        call_data_bin = decode_hex(call_data)
        arg_types = self.func_map.get(call_data_bin[:4])
        if arg_types:
            return decode_abi(arg_types, call_data_bin[4:])