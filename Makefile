WORK_DIR := $(shell pwd)

run_neo4j:
	docker run -d --publish=7474:7474 --publish=7687:7687 --volume=$(WORK_DIR)/.neo4j/data:/data --volume=$(WORK_DIR)/.neo4j/logs:/logs neo4j:3.0