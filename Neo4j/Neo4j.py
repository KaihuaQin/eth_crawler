from py2neo.data import Node, Relationship
from py2neo.database import Graph
import time

class Neo4j(object):
    def __init__(self, uri='bolt://localhost:7687', user='neo4j', password='Neo4j'):
        self._graph = Graph(uri, auth=(user, password),)

    def save_idex_trade(self, trade_info, blocknum):
        """
        MERGE (left:Trader { address: $left })
        MERGE (right:Trader { address: $right })
        CREATE (left)-[l2r:$token_l2r { amount: $amount_l2r }]->(right)
        CREATE (right)-[r2l:$token_r2l { amount: $amount_r2l }]->(left)
        """
        amountBuy, amountSell, _, _, amount_l2r, _, _, _ = trade_info[0]
        token_l2r, token_r2l, right, left = trade_info[1]
        amount_r2l = amount_l2r*amountSell/amountBuy
        # Using Finney
        amount_l2r /= 1000
        amount_r2l /= 1000
        tx = self._graph.begin()
        tx.run("MERGE (left:Trader { address: $left }) "
               "MERGE (right:Trader { address: $right }) "
               "CREATE (left)-[l2r:Transfer { token: $token_l2r, amount: $amount_l2r, "
               "exchange: \"IDEX\", blocknum: $blocknum, inExchangeOf: $token_r2l, "
               "price: $price_l2r }]->(right) "
               "CREATE (right)-[r2l:Transfer { token: $token_r2l, amount: $amount_r2l, "
               "exchange: \"IDEX\", blocknum: $blocknum, inExchangeOf: $token_l2r, "
               "price: $price_r2l}]->(left) ",
               left=left, right=right, amount_l2r=amount_l2r, amount_r2l=amount_r2l,
               token_l2r=token_l2r, token_r2l=token_r2l, price_l2r=amount_l2r/amount_r2l,
               price_r2l=amount_r2l/amount_l2r, blocknum=blocknum)
        tx.commit()