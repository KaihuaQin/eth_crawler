from Ethereum.Crawler import EthereumCrawler
from Ethereum.Decoder import InputDecoder
from Ethereum.Filter import Pattern, matchPattern
from Neo4j.Neo4j import Neo4j
from tqdm import tqdm

if __name__ == "__main__":
    IDEX_patterns = []
    IDEX_patterns.append(Pattern(key='to',
                                 value='0x2a0c0dbecc7e4d658f48e01e3fa353f44050c208'))
    IDEX_decoder = InputDecoder('IDEX_0x2a0c0dbecc7e4d658f48e01e3fa353f44050c208_abi.json')

    crawler = EthereumCrawler()

    start_block = 4317141
    latest_block = crawler.getBlockNumber()

    neo = Neo4j()

    for num in tqdm(range(6700662, 6700662+200)):#latest_block)):
        block = crawler.getBlockByNumber(num)
        blocknum = int(block['number'],16)
        transactions = block['transactions']
        for t in transactions:
            if matchPattern(t, IDEX_patterns) and IDEX_decoder.get_func_name(t['input']) == 'trade':
                trade_info = IDEX_decoder.decode_call(t['input'])
                neo.save_idex_trade(trade_info, blocknum)